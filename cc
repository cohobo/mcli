#!/bin/bash
pwd=$(pwd)
b/utils/hook before "$0" "$@"
b/m cache:clean "$@"
cd "$pwd" && b/utils/hook after "$0" "$@"
