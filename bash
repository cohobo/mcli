#!/bin/bash
source b/conf/.env
pwd=$(pwd)
b/utils/hook before "$0" "$@"
cd $DOCKER_DIR && docker-compose exec -u www-data $APP_CONTAINER bash
cd "$pwd" && b/utils/hook after "$0" "$@"
