#!/bin/bash
pwd=$(pwd)
b/utils/hook before "$0" "$@"
if [ "config:set" == "$1" ]; then
  echo "Setting config value ${2} to ${3}"
fi
b/cli bin/magento "$@"
b/utils/hook after "$0" "$@"
cd "$pwd" && b/utils/hook after "$0" "$@"
