# Instalation
Place project in root of your magento project, then
create .env file in conf directory (see .env.dist).
Adjust variables to match your project settings

# Usage
Use provide scripts calling them from your project root i.e.
```bash
b/start
```

## Some usage examplses
### Managing containers
```bash
b/start
```
```bash
b/stop
```
```bash
b/restart
```

### Entering contaier
As normal user
```bash
b/bash
```
As root user
```bash
b/root
```

### Enabling/Disabling xdebug
```bash
b/xdebug enable
```
```bash
b/xdebug disable
```

### Magento specific commands
Calling magento command
```bash
b/m [some magento command ie. setup:upgrade]
```
Clearing cache
```bash
b/cc
```
Flushing cache + removing generated/code/*
```bash
b/ccc
```
Setup upgrade
```bash
b/setupupgrade
```

### Executing cli command in contaier
```bash
b/cli [command i.e. php -v]
```

# Stages & hooks
Each script has configured before/after hooks allowing you to execute some extra logic
before or after script execution. Hooks can be defined in stages.

## Defining stages
In .env file add two variables
```bash
#path to dir with stages
STAGES_DIR=/home/hadwao/www/my_project/.stages
#stage name for custom hooks etc
STAGE=local
```

In $STAGES_DIR add directory structure like bellow:
```
--./home/hadwao/www/my_project/.stages (defined in $STAGES_DIR variable)
  |-- common
    |-- hook
      |-- before
      |-- after
  |-- local (defined in $STAGE variable)
    |-- hook
      |-- before
      |-- after
```

In common stage place all scripts that you want to execute on all environments.
In $STAGE directory place scripts unique to given development environment (this way you can have multiple
 specific configurations stored in a repository);
 
## Using stages & hooks

Let's create two files:

../common/hook/after/syncdb with content:
```bash
b/m config:set admin/security/session_lifetime 31536000
```

and

../local/hook/after/syncdb with content:
```bash
b/m admin:user:create --admin-user="testadmin" --admin-password="testadmin24" --admin-email="test@example.com" --admin-firstname="Test" --admin-lastname="Admin"
b/m config:set smtp/configuration_option/host mailhogserver
b/m config:set smtp/configuration_option/port 1025
b/m config:set smtp/configuration_option/authentication plain
b/m config:set smtp/configuration_option/protocol ''
```

This way every time you synchronize a database you also extend admin session to one year.
As this scripts is placed in common area it is executed for all users.

Additionally, for users using local stage second script is executed - it creates 
testadmin user and configure local mailhog server.

