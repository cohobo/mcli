#!/bin/bash
pwd=$(pwd)
b/utils/hook before "$0" "$@"
cd docker && docker-compose up -d
cd "$pwd" && b/utils/hook after "$0" "$@"
